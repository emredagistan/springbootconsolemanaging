import com.example.springbootconsole.SpringBootConsoleApplication;
import com.example.springbootconsoleduplicate.SpringBootConsoleDuplicateApplication;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Scanner;

public class SpringConsole {

    public static void main(String[] args) {

        while(true){

            Scanner sc = new Scanner(System.in);
            System.out.println("Enter 'X' to start service || Enter 'Y' to start duplicate service");
            String start = sc.next();

            if(start.equals("x") || start.equals("X")){

                System.out.println("Service starting...");

                SpringApplication sa = new SpringApplication(SpringBootConsoleApplication.class);
                sa.setBannerMode(Banner.Mode.OFF);
                ConfigurableApplicationContext ctx = sa.run(args);

                System.out.println("Service online");

                System.out.println("Enter 'Q' to exit service");

                String exit = sc.next();

                if(exit.equals("q") || exit.equals("Q")){
                    System.out.println("Service closing...");
                    ctx.close();
                    System.out.println("Service offline");
                }

            }
            else if (start.equals("y") || start.equals("Y")){

                System.out.println("Duplicate service starting...");

                SpringApplication sa = new SpringApplication(SpringBootConsoleDuplicateApplication.class);
                sa.setBannerMode(Banner.Mode.OFF);
                ConfigurableApplicationContext ctx = sa.run(args);

                System.out.println("Duplicate service online");

                System.out.println("Enter 'Q' to exit duplicate service");

                String exit = sc.next();

                if(exit.equals("q") || exit.equals("Q")){
                    System.out.println("Duplicate service closing...");
                    ctx.close();
                    System.out.println("Duplicate service offline");
                }

            }

        }

    }

}
